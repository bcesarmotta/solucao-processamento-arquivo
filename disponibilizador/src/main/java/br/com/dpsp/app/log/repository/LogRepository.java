package br.com.dpsp.app.log.repository;

import br.com.dpsp.app.log.dto.LogDTO;
import br.com.dpsp.app.log.mapper.LogResultSetExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class LogRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<LogDTO> findAll() {		
		return jdbcTemplate.query("SELECT * FROM TB_LOG_TRACE_ORDERS_GERMINAR",
				LogResultSetExtractor.listExtractor());
	}

	public LogDTO findBySequencia(String sequencia) {
		return jdbcTemplate.query("SELECT * FROM TB_LOG_TRACE_ORDERS_GERMINAR " + " WHERE SEQUENCIA_TRANSACAO = ?",
				new Object[] { sequencia }, LogResultSetExtractor.entityExtractor());
	}

	public LogDTO findBySequenciaTransacao(String sequencia, String origem) {
		return jdbcTemplate.query("SELECT * FROM TB_LOG_TRACE_ORDERS_GERMINAR " + " WHERE SEQUENCIA_TRANSACAO = ? AND ORIGEM = ?",
				new Object[] { sequencia, origem }, LogResultSetExtractor.entityExtractor());
	}
}
