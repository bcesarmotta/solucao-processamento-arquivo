package br.com.dpsp.app.log.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@EqualsAndHashCode(of = { "id" })
public class LogDTO {

	private static final String DATE_FORMAT = "dd/MM/yyyy";

	private BigDecimal id;
	private String sequencia;
	private String integrador;
	private String nomeTransacao;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
	private LocalDate inicioTransacao;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
	private LocalDate fimTransacao;
	private String origem;
	private String destino;
	private String descricao;
	private String status;
	private String detalhes;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
	private LocalDate dataEmissao;

	public LogDTO() {

	}

	public LogDTO(BigDecimal id, String sequencia, String nomeTransacao, String integrador, LocalDate inicioTransacao,
			LocalDate fimTransacao, String origem, String destino, String descricao, String status, String detalhes) {
		this.id = id;
		this.sequencia = sequencia;
		this.nomeTransacao = nomeTransacao;
		this.integrador = integrador;
		this.inicioTransacao = inicioTransacao;
		this.fimTransacao = fimTransacao;
		this.origem = origem;
		this.destino = destino;
		this.descricao = descricao;
		this.status = status;
		this.detalhes = detalhes;

	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getSequencia() {
		return sequencia;
	}

	public void setSequencia(String sequencia) {
		this.sequencia = sequencia;
	}

	public String getIntegrador() {
		return integrador;
	}

	public void setIntegrador(String integrador) {
		this.integrador = integrador;
	}

	public String getNomeTransacao() {
		return nomeTransacao;
	}

	public void setNomeTransacao(String nomeTransacao) {
		this.nomeTransacao = nomeTransacao;
	}

	public LocalDate getInicioTransacao() {
		return inicioTransacao;
	}

	public void setInicioTransacao(LocalDate inicioTransacao) {
		this.inicioTransacao = inicioTransacao;
	}

	public LocalDate getFimTransacao() {
		return fimTransacao;
	}

	public void setFimTransacao(LocalDate fimTransacao) {
		this.fimTransacao = fimTransacao;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}

	public LocalDate getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(LocalDate dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public static String getDateFormat() {
		return DATE_FORMAT;
	}

}
